//[SECTION] MODULES AND DEPENDENCIES
	const mongoose = require('mongoose');

//[SECTION] SCHEMA
	const orderSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: false
		},
		products: [{
			productId: {
				type: String,
				required: [true, 'is required']
			},
			quantity: {
				type: Number,
				required: [true, 'is required']
			}
		}],
		totalAmount: {
			type: Number,
			required: false
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	});

//[SECTION] MODEL
	module.exports = mongoose.model('Orders', orderSchema);